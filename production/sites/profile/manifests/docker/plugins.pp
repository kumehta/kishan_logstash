# docker::plugins
class profile::docker::plugins($plugins) {
  create_resources(docker::plugin, $plugins)
}
