# docker::images
class profile::docker::images($images) {
  create_resources(profile::docker::image, $images)
}
