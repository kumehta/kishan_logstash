
class profile::docker::compose(
  Optional[Pattern[/^present$|^absent$/]] $ensure          = 'present',
  Optional[String] $version                                = $::profile::docker::params::compose_version,
  Optional[String] $install_path                           = $::profile::docker::params::compose_install_path,
  Optional[String] $proxy                                  = undef
) inherits profile::docker::params {

  if $proxy != undef {
      validate_re($proxy, '^((http[s]?)?:\/\/)?([^:^@]+:[^:^@]+@|)([\da-z\.-]+)\.([\da-z\.]{2,6})(:[\d])?([\/\w \.-]*)*\/?$')
  }

  if $ensure == 'present' {
    ensure_packages(['curl'])

    if $proxy != undef {
        $proxy_opt = "--proxy ${proxy}"
    } else {
        $proxy_opt = ''
    }

    exec { "Install Docker Compose ${version}":
      path    => '/usr/bin/',
      cwd     => '/tmp',
      command => "curl -s -S -L ${proxy_opt} https://github.com/docker/compose/releases/download/${version}/docker-compose-${::kernel}-x86_64 -o ${install_path}/docker-compose-${version}",
      creates => "${install_path}/docker-compose-${version}",
      require => Package['curl'],
    }

    file { "${install_path}/docker-compose-${version}":
      owner   => 'root',
      mode    => '0755',
      require => Exec["Install Docker Compose ${version}"]
    }

    file { "${install_path}/docker-compose":
      ensure  => 'link',
      target  => "${install_path}/docker-compose-${version}",
      require => File["${install_path}/docker-compose-${version}"]
    }
  } else {
    file { [
      "${install_path}/docker-compose-${version}",
      "${install_path}/docker-compose"
    ]:
      ensure => absent,
    }
  }
}
