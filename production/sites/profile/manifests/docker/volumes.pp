# profile::docker::volumes
class profile::docker::volumes($volumes) {
  create_resources(docker_volumes, $volumes)
}
