# == Class: profile::docker::service
#
# Class to manage the docker service daemon
#
# === Parameters
# [*tcp_bind*]
#   Which tcp port, if any, to bind the docker service to.
#
# [*ip_forward*]
#   This flag interacts with the IP forwarding setting on
#   your host system's kernel
#
# [*iptables*]
#   Enable Docker's addition of iptables rules
#
# [*ip_masq*]
#   Enable IP masquerading for bridge's IP range.
#
# [*socket_bind*]
#   Which local unix socket to bind the docker service to.
#
# [*socket_group*]
#   Which local unix socket to bind the docker service to.
#
# [*root_dir*]
#   Specify a non-standard root directory for docker.
#
# [*extra_parameters*]
#   Plain additional parameters to pass to the docker daemon
#
# [*shell_values*]
#   Array of shell values to pass into init script config files
#
# [*manage_service*]
#   Specify whether the service should be managed.
#   Valid values are 'true', 'false'.
#   Defaults to 'true'.
#
class profile::docker::service (
  $docker_command                    = $::profile::docker::docker_command,
  $docker_start_command              = $::profile::docker::docker_start_command,
  $service_name                      = $::profile::docker::service_name,
  $tcp_bind                          = $::prfile::docker::tcp_bind,
  $ip_forward                        = $::profile::docker::ip_forward,
  $iptables                          = $::profile::docker::iptables,
  $ip_masq                           = $::profile::docker::ip_masq,
  $icc                               = $::profile::docker::icc,
  $bridge                            = $::profile::docker::bridge,
  $fixed_cidr                        = $::profile::docker::fixed_cidr,
  $default_gateway                   = $::profile::docker::default_gateway,
  $ipv6                              = $::profile::docker::ipv6,
  $ipv6_cidr                         = $::profile::docker::ipv6_cidr,
  $default_gateway_ipv6              = $::profile::docker::default_gateway_ipv6,
  $socket_bind                       = $::profile::docker::socket_bind,
  $log_level                         = $::profile::docker::log_level,
  $log_driver                        = $::profile::docker::log_driver,
  $log_opt                           = $::profile::docker::log_opt,
  $selinux_enabled                   = $::profile::docker::selinux_enabled,
  $socket_group                      = $::profile::docker::socket_group,
  $labels                            = $::profile::docker::labels,
  $dns                               = $::profile::docker::dns,
  $dns_search                        = $::profile::docker::dns_search,
  $service_state                     = $::profile::docker::service_state,
  $service_enable                    = $::profile::docker::service_enable,
  $manage_service                    = $::profile::docker::manage_service,
  $root_dir                          = $::profile::docker::root_dir,
  $extra_parameters                  = $::profile::docker::extra_parameters,
  $shell_values                      = $::profile::docker::shell_values,
  $proxy                             = $::profile::docker::proxy,
  $no_proxy                          = $::profile::docker::no_proxy,
  $execdriver                        = $::profile::docker::execdriver,
  $bip                               = $::profile::docker::bip,
  $mtu                               = $::profile::docker::mtu,
  $storage_driver                    = $::profile::docker::storage_driver,
  $dm_basesize                       = $::profile::docker::dm_basesize,
  $dm_fs                             = $::profile::docker::dm_fs,
  $dm_mkfsarg                        = $::profile::docker::dm_mkfsarg,
  $dm_mountopt                       = $::profile::docker::dm_mountopt,
  $dm_blocksize                      = $::profile::docker::dm_blocksize,
  $dm_loopdatasize                   = $::profile::docker::dm_loopdatasize,
  $dm_loopmetadatasize               = $::profile::docker::dm_loopmetadatasize,
  $dm_datadev                        = $::profile::docker::dm_datadev,
  $dm_metadatadev                    = $::profile::docker::dm_metadatadev,
  $tmp_dir_config                    = $::profile::docker::tmp_dir_config,
  $tmp_dir                           = $::profile::docker::tmp_dir,
  $dm_thinpooldev                    = $::profile::docker::dm_thinpooldev,
  $dm_use_deferred_removal           = $::profile::docker::dm_use_deferred_removal,
  $dm_use_deferred_deletion          = $::profile::docker::dm_use_deferred_deletion,
  $dm_blkdiscard                     = $::profile::docker::dm_blkdiscard,
  $dm_override_udev_sync_check       = $::profile::docker::dm_override_udev_sync_check,
  $overlay2_override_kernel_check    = $::profile::docker::overlay2_override_kernel_check,
  $storage_devs                      = $::profile::docker::storage_devs,
  $storage_vg                        = $::profile::docker::storage_vg,
  $storage_root_size                 = $::profile::docker::storage_root_size,
  $storage_data_size                 = $::profile::docker::storage_data_size,
  $storage_min_data_size             = $::profile::docker::storage_min_data_size,
  $storage_chunk_size                = $::profile::docker::storage_chunk_size,
  $storage_growpart                  = $::profile::docker::storage_growpart,
  $storage_auto_extend_pool          = $::profile::docker::storage_auto_extend_pool,
  $storage_pool_autoextend_threshold = $::profile::docker::storage_pool_autoextend_threshold,
  $storage_pool_autoextend_percent   = $::profile::docker::storage_pool_autoextend_percent,
  $storage_config                    = $::profile::docker::storage_config,
  $storage_config_template           = $::profile::docker::storage_config_template,
  $storage_setup_file                = $::profile::docker::storage_setup_file,
  $service_provider                  = $::profile::docker::service_provider,
  $service_config                    = $::profile::docker::service_config,
  $service_config_template           = $::profile::docker::service_config_template,
  $service_overrides_template        = $::profile::docker::service_overrides_template,
  $service_hasstatus                 = $::profile::docker::service_hasstatus,
  $service_hasrestart                = $::profile::docker::service_hasrestart,
  $daemon_environment_files          = $::profile::docker::daemon_environment_files,
  $tls_enable                        = $::profile::docker::tls_enable,
  $tls_verify                        = $::profile::docker::tls_verify,
  $tls_cacert                        = $::profile::docker::tls_cacert,
  $tls_cert                          = $::profile::docker::tls_cert,
  $tls_key                           = $::profile::docker::tls_key,
  $registry_mirror                   = $::profile::docker::registry_mirror,
) {

  unless $::osfamily =~ /(Debian|RedHat)/ {
    fail translate(('The docker::service class needs a Debian or Redhat based system.'))
  }

  $dns_array = any2array($dns)
  $dns_search_array = any2array($dns_search)
  $labels_array = any2array($labels)
  $extra_parameters_array = any2array($extra_parameters)
  $shell_values_array = any2array($shell_values)
  $tcp_bind_array = any2array($tcp_bind)

  if $service_config != undef {
    $_service_config = $service_config
  } else {
    if $::osfamily == 'Debian' {
      $_service_config = "/etc/default/${service_name}"
    }
  }

  $_manage_service = $manage_service ? {
    true    => Service['docker'],
    default => [],
  }

  if $::osfamily == 'RedHat' {
    file { $storage_setup_file:
      ensure  => present,
      force   => true,
      content => template('docker/etc/sysconfig/docker-storage-setup.erb'),
      before  => $_manage_service,
      notify  => $_manage_service,
    }
  }

  case $service_provider {
    'systemd': {
      file { '/etc/systemd/system/docker.service.d':
        ensure => directory,
      }

      if $service_overrides_template {
        file { '/etc/systemd/system/docker.service.d/service-overrides.conf':
          ensure  => present,
          content => template($service_overrides_template),
          notify  => Exec['docker-systemd-reload-before-service'],
          before  => $_manage_service,
        }
        exec { 'docker-systemd-reload-before-service':
          path        => ['/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/'],
          command     => 'systemctl daemon-reload > /dev/null',
          before      => $_manage_service,
          refreshonly => true,
        }
      }
    }
    'upstart': {
      file { '/etc/init.d/docker':
        ensure => 'link',
        target => '/lib/init/upstart-job',
        force  => true,
        notify => $_manage_service,
      }
    }
    default: {}
  }

  if $storage_config {
    file { $storage_config:
      ensure  => present,
      force   => true,
      content => template($storage_config_template),
      notify  => $_manage_service,
    }
  }

  if $_service_config {
    file { $_service_config:
      ensure  => present,
      force   => true,
      content => template($service_config_template),
      notify  => $_manage_service,
    }
  }

  if $manage_service {
    if ! defined(Service['docker']) {
      service { 'docker':
        ensure     => $service_state,
        name       => $service_name,
        enable     => $service_enable,
        hasstatus  => $service_hasstatus,
        hasrestart => $service_hasrestart,
        provider   => $service_provider,
      }
    }
  }
}
