class profile::docker::install {
  $docker_start_command = $::profile::docker::docker_start_command
  if $::osfamily {
    assert_type(Pattern[/^(Debian|RedHat)$/], $::osfamily) |$a, $b| {
      fail translate(('This module only works on Debian or RedHat.'))
    }
  }
  if $::profile::docker::version and $::profile::docker::ensure != 'absent' {
    $ensure = $::profile::docker::version
  } else {
    $ensure = $::profile::docker::ensure
  }

  if $::profile::docker::manage_package {

    if empty($::profile::docker::repo_opt) {
      $docker_hash = {}
    } else {
      $docker_hash = { 'install_options' => $docker::repo_opt }
    }

    if $::profile::docker::package_source {
      case $::osfamily {
        'Debian' : {
          $pk_provider = 'dpkg'
        }
        'RedHat' : {
          $pk_provider = 'rpm'
        }
        default : {
          $pk_provider = undef
        }
      }

      case $::profile::docker::package_source {
        /docker-engine/ : {
          ensure_resource('package', 'docker', merge($docker_hash, {
            ensure   => $ensure,
            provider => $pk_provider,
            source   => $::profile::docker::package_source,
            name     => $::profile::docker::docker_engine_package_name,
          }))
        }
        /docker-ce/ : {
          ensure_resource('package', 'docker', merge($docker_hash, {
            ensure   => $ensure,
            provider => $pk_provider,
            source   => $::profile::docker::package_source,
            name     => $::profile::docker::docker_ce_package_name,
          }))
        }
        default : {}
      }

    } else {
      ensure_resource('package', 'docker', merge($docker_hash, {
        ensure => $ensure,
        name   => $::profile::docker::docker_package_name,
      }))
    }
  }
}
