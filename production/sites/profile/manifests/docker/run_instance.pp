# docker::run_instance
class profile::docker::run_instance($instance) {
  create_resources(docker::run, $instance)
}
