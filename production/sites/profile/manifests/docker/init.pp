class profile::docker::init (
  Optional[String] $version                                 = $::profile::docker::params::version,
  String $ensure                                            = $::profile::docker::params::ensure,
  Variant[Array[String], Hash] $prerequired_packages        = $::profile::docker::params::prerequired_packages,
  String $docker_ce_start_command                           = $::profile::docker::params::docker_ce_start_command,
  Optional[String] $docker_ce_package_name                  = $::profile::docker::params::docker_ce_package_name,
  Optional[String] $docker_ce_source_location               = $::profile::docker::params::package_ce_source_location,
  Optional[String] $docker_ce_key_source                    = $::profile::docker::params::package_ce_key_source,
  Optional[String] $docker_ce_key_id                        = $::profile::docker::params::package_ce_key_id,
  Optional[String] $docker_ce_release                       = $::profile::docker::params::package_ce_release,
  Optional[String] $docker_package_location                 = $::profile::docker::params::package_source_location,
  Optional[String] $docker_package_key_source               = $::profile::docker::params::package_key_source,
  Optional[Boolean] $docker_package_key_check_source        = $::profile::docker::params::package_key_check_source,
  Optional[String] $docker_package_key_id                   = $::profile::docker::params::package_key_id,
  Optional[String] $docker_package_release                  = $::profile::docker::params::package_release,
  String $docker_engine_start_command                       = $::profile::docker::params::docker_engine_start_command,
  String $docker_engine_package_name                        = $::profile::docker::params::docker_engine_package_name,
  String $docker_ce_channel                                 = $::profile::docker::params::docker_ce_channel,
  Optional[Boolean] $docker_ee                              = $::profile::docker::params::docker_ee,
  Optional[String] $docker_ee_package_name                  = $::profile::docker::params::package_ee_package_name,
  Optional[String] $docker_ee_source_location               = $::profile::docker::params::package_ee_source_location,
  Optional[String] $docker_ee_key_source                    = $::profile::docker::params::package_ee_key_source,
  Optional[String] $docker_ee_key_id                        = $::profile::docker::params::package_ee_key_id,
  Optional[String] $docker_ee_repos                         = $::profile::docker::params::package_ee_repos,
  Optional[String] $docker_ee_release                       = $::profile::docker::params::package_ee_release,
  Variant[String,Array[String],Undef] $tcp_bind             = $::profile::docker::params::tcp_bind,
  Boolean $tls_enable                                       = $::profile::docker::params::tls_enable,
  Boolean $tls_verify                                       = $::profile::docker::params::tls_verify,
  Optional[String] $tls_cacert                              = $::profile::docker::params::tls_cacert,
  Optional[String] $tls_cert                                = $::profile::docker::params::tls_cert,
  Optional[String] $tls_key                                 = $::profile::docker::params::tls_key,
  Boolean $ip_forward                                       = $::profile::docker::params::ip_forward,
  Boolean $ip_masq                                          = $::profile::docker::params::ip_masq,
  Optional[Boolean]$ipv6                                    = $::profile::docker::params::ipv6,
  Optional[String]$ipv6_cidr                                = $::profile::docker::params::ipv6_cidr,
  Optional[String]$default_gateway_ipv6                     = $::profile::docker::params::default_gateway_ipv6,
  Optional[String] $bip                                     = $::profile::docker::params::bip,
  Optional[String] $mtu                                     = $::profile::docker::params::mtu,
  Boolean $iptables                                         = $::profile::docker::params::iptables,
  Optional[Boolean] $icc                                    = $::profile::docker::params::icc,
  String $socket_bind                                       = $::profile::docker::params::socket_bind,
  Optional[String] $fixed_cidr                              = $::profile::docker::params::fixed_cidr,
  Optional[String] $bridge                                  = $::profile::docker::params::bridge,
  Optional[String] $default_gateway                         = $::profile::docker::params::default_gateway,
  Optional[String] $log_level                               = $::profile::docker::params::log_level,
  Optional[String] $log_driver                              = $::profile::docker::params::log_driver,
  Array $log_opt                                            = $::profile::docker::params::log_opt,
  Optional[Boolean] $selinux_enabled                        = $::profile::docker::params::selinux_enabled,
  Optional[Boolean] $use_upstream_package_source            = $::profile::docker::params::use_upstream_package_source,
  Optional[Boolean] $pin_upstream_package_source            = $::profile::docker::params::pin_upstream_package_source,
  Optional[Integer] $apt_source_pin_level                   = $::profile::docker::params::apt_source_pin_level,
  Optional[String] $package_release                         = $::profile::docker::params::package_release,
  String $service_state                                     = $::profile::docker::params::service_state,
  Boolean $service_enable                                   = $::profile::docker::params::service_enable,
  Boolean $manage_service                                   = $::profile::docker::params::manage_service,
  Optional[String] $root_dir                                = $::profile::docker::params::root_dir,
  Optional[Boolean] $tmp_dir_config                         = $::profile::docker::params::tmp_dir_config,
  Optional[String] $tmp_dir                                 = $::profile::docker::params::tmp_dir,
  Variant[String,Array,Undef] $dns                          = $::profile::docker::params::dns,
  Variant[String,Array,Undef] $dns_search                   = $::profile::docker::params::dns_search,
  Optional[String] $socket_group                            = $::profile::docker::params::socket_group,
  Array $labels                                             = $::profile::docker::params::labels,
  Variant[String,Array,Undef] $extra_parameters             = undef,
  Variant[String,Array,Undef] $shell_values                 = undef,
  Optional[String] $proxy                                   = $::profile::docker::params::proxy,
  Optional[String] $no_proxy                                = $::profile::docker::params::no_proxy,
  Optional[String] $storage_driver                          = $::profile::docker::params::storage_driver,
  Optional[String] $dm_basesize                             = $::profile::docker::params::dm_basesize,
  Optional[String] $dm_fs                                   = $::profile::docker::params::dm_fs,
  Optional[String] $dm_mkfsarg                              = $::profile::docker::params::dm_mkfsarg,
  Optional[String] $dm_mountopt                             = $::profile::docker::params::dm_mountopt,
  Optional[String] $dm_blocksize                            = $::profile::docker::params::dm_blocksize,
  Optional[String] $dm_loopdatasize                         = $::profile::docker::params::dm_loopdatasize,
  Optional[String] $dm_loopmetadatasize                     = $::profile::docker::params::dm_loopmetadatasize,
  Optional[String] $dm_datadev                              = $::profile::docker::params::dm_datadev,
  Optional[String] $dm_metadatadev                          = $::profile::docker::params::dm_metadatadev,
  Optional[String] $dm_thinpooldev                          = $::profile::docker::params::dm_thinpooldev,
  Optional[Boolean] $dm_use_deferred_removal                = $::profile::docker::params::dm_use_deferred_removal,
  Optional[Boolean] $dm_use_deferred_deletion               = $::profile::docker::params::dm_use_deferred_deletion,
  Optional[Boolean] $dm_blkdiscard                          = $::profile::docker::params::dm_blkdiscard,
  Optional[Boolean] $dm_override_udev_sync_check            = $::profile::docker::params::dm_override_udev_sync_check,
  Boolean $overlay2_override_kernel_check                   = $::profile::docker::params::overlay2_override_kernel_check,
  Optional[String] $execdriver                              = $::profile::docker::params::execdriver,
  Boolean $manage_package                                   = $::profile::docker::params::manage_package,
  Optional[String] $package_source                          = $::profile::docker::params::package_source,
  Optional[String] $service_name                            = $::profile::docker::params::service_name,
  Array $docker_users                                       = [],
  String $docker_group                                      = $::profile::docker::params::docker_group,
  Array $daemon_environment_files                           = [],
  Variant[String,Hash,Undef] $repo_opt                      = $::profile::docker::params::repo_opt,
  Optional[String] $os_lc                                   = $::profile::docker::params::os_lc,
  Optional[String] $storage_devs                            = $::profile::docker::params::storage_devs,
  Optional[String] $storage_vg                              = $::profile::docker::params::storage_vg,
  Optional[String] $storage_root_size                       = $::profile::docker::params::storage_root_size,
  Optional[String] $storage_data_size                       = $::profile::docker::params::storage_data_size,
  Optional[String] $storage_min_data_size                   = $::profile::docker::params::storage_min_data_size,
  Optional[String] $storage_chunk_size                      = $::profile::docker::params::storage_chunk_size,
  Optional[Boolean] $storage_growpart                       = $::profile::docker::params::storage_growpart,
  Optional[String] $storage_auto_extend_pool                = $::profile::docker::params::storage_auto_extend_pool,
  Optional[String] $storage_pool_autoextend_threshold       = $::profile::docker::params::storage_pool_autoextend_threshold,
  Optional[String] $storage_pool_autoextend_percent         = $::profile::docker::params::storage_pool_autoextend_percent,
  Variant[String,Boolean,Undef] $storage_config             = $::profile::docker::params::storage_config,
  Optional[String] $storage_config_template                 = $::profile::docker::params::storage_config_template,
  Optional[String] $storage_setup_file                      = $::profile::docker::params::storage_setup_file,
  Optional[String] $service_provider                        = $::profile::docker::params::service_provider,
  Variant[String,Boolean,Undef] $service_config             = $::profile::docker::params::service_config,
  Optional[String] $service_config_template                 = $::profile::docker::params::service_config_template,
  Variant[String,Boolean,Undef] $service_overrides_template = $::profile::docker::params::service_overrides_template,
  Optional[Boolean] $service_hasstatus                      = $::profile::docker::params::service_hasstatus,
  Optional[Boolean] $service_hasrestart                     = $::profile::docker::params::service_hasrestart,
  Optional[String] $registry_mirror                         = $::profile::docker::params::registry_mirror,
) inherits profile::docker::params {


  if $::osfamily {
    assert_type(Pattern[/^(Debian|RedHat)$/], $::osfamily) |$a, $b| {
      fail translate(('This module only works on Debian or Red Hat based systems.'))
    }
  }

  if ($default_gateway) and (!$bridge) {
    fail translate(('You must provide the $bridge parameter.'))
  }

  if $log_level {
    assert_type(Pattern[/^(debug|info|warn|error|fatal)$/], $log_level) |$a, $b| {
        fail translate(('log_level must be one of debug, info, warn, error or fatal'))
    }
  }

  if $log_driver {
    assert_type(Pattern[/^(none|json-file|syslog|journald|gelf|fluentd|splunk)$/], $log_driver) |$a, $b| {
      fail translate(('log_driver must be one of none, json-file, syslog, journald, gelf, fluentd or splunk'))
    }
  }

  if $storage_driver {
    assert_type(Pattern[/^(aufs|devicemapper|btrfs|overlay|overlay2|vfs|zfs)$/], $storage_driver) |$a, $b| {
      fail translate(('Valid values for storage_driver are aufs, devicemapper, btrfs, overlay, overlay2, vfs, zfs.'))
    }
  }

  if $dm_fs {
    assert_type(Pattern[/^(ext4|xfs)$/], $dm_fs) |$a, $b| {
      fail translate(('Only ext4 and xfs are supported currently for dm_fs.'))
    }
  }

  if ($dm_loopdatasize or $dm_loopmetadatasize) and ($dm_datadev or $dm_metadatadev) {
    fail translate(('You should provide parameters only for loop lvm or direct lvm, not both.'))
  }

# lint:ignore:140chars
  if ($dm_datadev or $dm_metadatadev) and $dm_thinpooldev {
    fail translate(('You can use the $dm_thinpooldev parameter, or the $dm_datadev and $dm_metadatadev parameter pair, but you cannot use both.'))
  }
# lint:endignore

  if ($dm_datadev or $dm_metadatadev) {
    notice('The $dm_datadev and $dm_metadatadev parameter pair are deprecated.  The $dm_thinpooldev parameter should be used instead.')
  }

  if ($dm_datadev and !$dm_metadatadev) or (!$dm_datadev and $dm_metadatadev) {
    fail translate(('You need to provide both $dm_datadev and $dm_metadatadev parameters for direct lvm.'))
  }

  if ($dm_basesize or $dm_fs or $dm_mkfsarg or $dm_mountopt or $dm_blocksize or $dm_loopdatasize or
      $dm_loopmetadatasize or $dm_datadev or $dm_metadatadev) and ($storage_driver != 'devicemapper') {
    fail translate(('Values for dm_ variables will be ignored unless storage_driver is set to devicemapper.'))
  }

  if($tls_enable) {
    if(!$tcp_bind) {
        fail translate(('You need to provide tcp bind parameter for TLS.'))
    }
  }

  if ( $version == undef ) or ( $version !~ /^(17[.]0[0-5][.]\d(~|-|\.)ce|1.\d+)/ ) {
    if ( $docker_ee) {
      $package_location = $::profile::docker::docker_ee_source_location
      $package_key_source = $::profile::docker::docker_ee_key_source
      $package_key_check_source = true
      $package_key = $::profile::docker::docker_ee_key_id
      $package_repos = $::profile::docker::docker_ee_repos
      $release = $::profile::docker::docker_ee_release
      $docker_start_command = $::profile::docker::docker_ee_start_command
      $docker_package_name = $::profile::docker::docker_ee_package_name
    } else {
        case $::osfamily {
          'Debian' : {
            $package_location = $docker_ce_source_location
            $package_key_source = $docker_ce_key_source
            $package_key = $docker_ce_key_id
            $package_repos = $docker_ce_channel
            $release = $docker_ce_release
            }
          'Redhat' : {
            $package_location = "https://download.docker.com/linux/centos/${::operatingsystemmajrelease}/${::architecture}/${docker_ce_channel}"
            $package_key_source = $docker_ce_key_source
            $package_key_check_source = true
          }
          default: {}
        }
        $docker_start_command = $docker_ce_start_command
        $docker_package_name = $docker_ce_package_name
    }
  } else {
    case $::osfamily {
      'Debian' : {
        $package_location = $docker_package_location
        $package_key_source = $docker_package_key_source
        $package_key_check_source = $docker_package_key_check_source
        $package_key = $docker_package_key_id
        $package_repos = 'main'
        $release = $docker_package_release
        }
      'Redhat' : {
        $package_location = $docker_package_location
        $package_key_source = $docker_package_key_source
        $package_key_check_source = $docker_package_key_check_source
      }
      default : {}
    }
    $docker_start_command = $docker_engine_start_command
    $docker_package_name = $docker_engine_package_name
  }

  contain 'profile::docker::repos'
  contain 'profile::docker::install'
  contain 'profile::docker::config'
  contain 'profile::docker::service'

  Class['profile::docker::repos'] -> Class['profile::docker::install'] -> Class['profile::docker::config'] ~> Class['profile::docker::service']
  Class['profile::docker'] -> Docker::Registry <||> -> Docker::Image <||> -> Docker::Run <||>
  Class['profile::docker'] -> Docker::Image <||> -> Docker::Run <||>
  Class['profile::docker'] -> Docker::Run <||>

}
