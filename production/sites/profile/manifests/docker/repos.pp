# == Class: docker::repos
#
#
class profile::docker::repos (
  $location         = $profile::docker::package_location,
  $key_source       = $profile::docker::package_key_source,
  $key_check_source = $profile::docker::package_key_check_source,
  $architecture     = $facts['architecture'],
  ) {

  ensure_packages($profile::docker::prerequired_packages)

  case $::osfamily {
    'Debian': {
      $release = $profile::docker::release
      $package_key = $profile::docker::package_key
      $package_repos = $profile::docker::package_repos
      if ($profile::docker::use_upstream_package_source) {
        ensure_packages(['debian-keyring', 'debian-archive-keyring'])

        apt::source { 'docker':
          location     => $location,
          architecture => $architecture,
          release      => $release,
          repos        => $package_repos,
          key          => {
            id     => $package_key,
            source => $key_source,
          },
          require      => Package['debian-keyring', 'debian-archive-keyring'],
          include      => {
            src => false,
            },
        }
        $url_split = split($location, '/')
        $repo_host = $url_split[2]
        $pin_ensure = $profile::docker::pin_upstream_package_source ? {
            true    => 'present',
            default => 'absent',
        }
        apt::pin { 'docker':
          ensure   => $pin_ensure,
          origin   => $repo_host,
          priority => $profile::docker::apt_source_pin_level,
        }
        if $profile::docker::manage_package {
          include apt
          if $::operatingsystem == 'Debian' and $::lsbdistcodename == 'wheezy' {
            include apt::backports
          }
          Exec['apt_update'] -> Package[$profile::docker::prerequired_packages]
          Apt::Source['docker'] -> Package['docker']
        }
      }

    }
    'RedHat': {

      if ($profile::docker::manage_package) {
          $baseurl = $location
          $gpgkey = $key_source
          $gpgkey_check = $key_check_source
        if ($profile::docker::use_upstream_package_source) {
          yumrepo { 'docker':
            descr    => 'Docker',
            baseurl  => $baseurl,
            gpgkey   => $gpgkey,
            gpgcheck => $gpgkey_check,
          }
          Yumrepo['docker'] -> Package['docker']
        }
      }
    }
    default: {}
  }
}
