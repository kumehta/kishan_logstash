# docker::registry_auth
class profile::docker::registry_auth($registries) {
  create_resources(docker::registry, $registries)
}
