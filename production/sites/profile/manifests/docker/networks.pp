# docker::networks
class profile::docker::networks($networks) {
  create_resources(docker_network, $networks)
}
