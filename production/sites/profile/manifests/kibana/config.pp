#
# 
#
class profile::kibana::config {

$ensure = lookup('profile::kibana::ensure', String, deep)

  $_ensure = $ensure ? {
    'absent' => $ensure,
    default  => 'file',
  }
 

  file { '/etc/kibana/kibana.yml':
    ensure  => $_ensure,
    content => inline_template("/etc/kibana/kibana.yml.erb"),
    owner   => 'kibana',
    group   => 'kibana',
    mode    => '0660',
  }
}

