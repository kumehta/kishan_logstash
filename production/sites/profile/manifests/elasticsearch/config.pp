# -----------------------------------------------------------------------------
# Author  = kishan Agarwal
# Date    = 2018/04/18
# Version = 1.1 'sftpplus/config.pp'
# Purpose = This is the Business Integration Gateway Platform
#           'sftpplus/config' profile.
# -----------------------------------------------------------------------------

class profile::elasticsearch::config {

  $owner = lookup('profile::elasticsearch::config::owner',String,deep)
  $group = lookup('profile::elasticsearch::config::group',String,deep)
  $config_dir = lookup('profile::elasticsearch::config::config_dir',String,deep)
  $template_elasticsearch = lookup('profile::elasticsearch::config::template_elasticsearch',String,deep)
  $template_logging = lookup('profile::elasticsearch::config::template_logging',String,deep)

  # Set the global execution path.
  Exec { path => ['/sbin', '/bin', '/usr/sbin', '/usr/bin', '/opt/sftpplus/bin'], }

  # Identify application package version.
  case $facts['os']['family'] {
    'RedHat': { $package = lookup('profile::elasticsearch::install::package_RHEL_elasticsearch', String, deep) }
  }

  # Setup the SFTPPlus server configuration directory.
  file { "$config_dir":
    ensure  => directory,
    owner   => "$owner",
    group   => "$group",
    mode    => "0775",
  } ->
  concat { "${config_dir}/elasticsearch.yml":
    owner   => "$owner",
    group   => "$group",
    mode    => "0640",
    require => File[$config_dir],
  } ->
  concat::fragment { "${config_dir}/elasticsearch.yml templated":
    target  => "${config_dir}/elasticsearch.yml}",
    content => inline_template($template_elasticsearch),
    order   => '01',
  } ->
  concat { "${config_dir}/logging.yml":
    owner   => "$owner",
    group   => "$group",
    mode    => "0640",
    require => File[$config_dir],
  } ->
  concat::fragment { "${config_dir}/logging.yml templated":
    target  => "${config_dir}/logging.yml}",
    content => inline_template($template_logging),
    order   => '01',
  } ->

  # Start the configured application service via System V run control.
  exec { "$package-ServiceStart":
    command => "/usr/bin/systemctl start elasticsearch",
    unless  => "ps -ef | grep elasticsearch | grep -v grep",
  } ->

  # Refresh the configured application service via System V run control.
  exec { "$package-ServiceRefresh":
    command     => "/usr/bin/systemctl restart elasticsearch",
    refreshonly => true,
    subscribe   => Concat["${config_dir}/elasticsearch.yml"],
  }

}

# -----------------------------------------------------------------------------
# End of file.

