# -------------------------------------------------------...
# -----------------------------------------------------------------------------
# Author  = Kishan Agarwal
#
# -----------------------------------------------------------------------------

class profile::elasticsearch::install {

  # Identify the Nexus Repository Server to be used.
  $nexus_server = lookup('profile::common::nexus', String, deep)
  $owner = lookup('profile::elasticsearch::config::owner', String, deep)
  $group = lookup('profile::elasticsearch::config::group', String, deep)
  $package_RHEL_elasticsearch= lookup('profile::elasticsearch::install::package_RHEL_elasticsearch')
  $nexus_repo_RHEL = lookup('profile::elasticsearch::install::nexus_repo_RHEL')

  # Set the global execution path.
  Exec { path => ['/sbin', '/bin', '/usr/sbin', '/usr/bin', '/opt/sftpplus/bin'], }

  # Create the installation packages directory.
  file { "/opt/packages":
    ensure  => directory,
    owner   => '0',
    group   => '0',
    mode    => '0755',
  } ->

  # Operating system dependent installation.
  case $facts['os']['family'] {

    'RedHat': {

      # Download the installation media.
      exec { "Download $package_RHEL_elasticsearch.tar.gz":
        command => "curl -k $nexus_server/$nexus_repo_RHEL/$package_RHEL_elasticsearch.tar.gz -o /opt/packages/$package_RHEL_elasticsearch.tar.gz",
        unless  => "test -d /opt/$package_RHEL_elasticsearch",
        require => File['/opt/packages'],
      } ->

      # Install the application code.
      exec { "$package_RHEL_elasticsearch-Install":
        command => "cd /opt && tar -zxvf /opt/packages/$package_RHEL_elasticsearch.tar.gz",
        creates => "/opt/$package_RHEL_elasticsearch",
        require => Exec["Download $package_RHEL_elasticsearch.tar.gz"],
      } ->

      # (Re)set application code file permissions.
      exec { "$package_RHEL_elasticsearch-Permissions":
        command => "chown -R $owner:$group /opt/$package_RHEL_elasticsearch && touch /opt/$package_RHEL_elasticsearch/.done.perms",
        creates => "/opt/$package_RHEL_elasticsearch/.done.perms",
      } ->

      # Create directory symlink to this package.
      file { "/opt/elasticsearch":
        ensure  => link,
        replace => yes,
        target  => "/opt/$package_RHEL_elasticsearch",
        owner   => $owner,
        group   => $group,
      } ->

      # Link the application start/stop elasticsearch.
      file { "/etc/rc.d/init.d/elasticsearch":
        ensure  => link,
        replace => yes,
        target  => "/opt/elasticsearch/bin/elasticsearch",
        owner   => '0',
        group   => '0',
      } ->

      # Delete the downloaded media file.
      exec { "Delete $package_RHEL_elasticsearch.tar.gz":
        command => "rm -f /opt/packages/$package_RHEL_elasticsearch.tar.gz",
        onlyif  => "test -f /opt/packages/$package_RHEL_elasticsearch.tar.gz",
      }
    }
  }
}

# -----------------------------------------------------------------------------
# End of file.
# -----------------------------------------------------------------------------


