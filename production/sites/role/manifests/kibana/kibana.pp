class role::kibana::kibana
{

class { profile::kibana::init: } ->
class { profile::kibana::config: } ->
class { profile::kibana::service: }


}

